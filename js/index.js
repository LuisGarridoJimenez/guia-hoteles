$(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 1000
    });

    var miid, dt, time;

    $(document).on('click', 'input[type="button"]', function(event) {
        miid = this.id;
    });


    $('#exampleModal').on('show.bs.modal', function(event) {
        dt = new Date();
        time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds() + ":" + dt
            .getMilliseconds();

        console.log(time + ' el modal comenzó a abrirse - evento show');
    });

    $('#exampleModal').on('shown.bs.modal', function(e) {
        dt = new Date();
        time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds() + ":" + dt
            .getMilliseconds();
        console.log(time + ' el modal se terminó de abrir - evento - shown');

        $('#' + miid).removeClass('btn-primary');
        $('#' + miid).addClass('btn-warning');
        $('#' + miid).prop('disabled');
    });

    $('#exampleModal').on('hide.bs.modal', function(e) {
        dt = new Date();
        time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds() + ":" + dt
            .getMilliseconds();

        console.log(time + ' el modal comienza a ocultarse - evento hide');
    });

    $('#exampleModal').on('hidden.bs.modal', function(e) {
        dt = new Date();
        time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds() + ":" + dt
            .getMilliseconds();

        console.log(time + ' el modal se terminó de ocultar evento - hiden');
        $('#' + miid).removeClass('btn-warning');
        $('#' + miid).addClass('btn-primary');
        $('#' + miid).prop('enabled');
    });
});